process.env.NODE_ENV = 'localDevelopment';
var express = require("express");
var http = require('http');
var path = require('path');
var config=require('config');
var bodyParser = require('body-parser');
var multipart = require('connect-multiparty');

var multipartMiddleware = multipart();
var app = express();


var imageUpload = require('./routes/users');
var index=require('./routes/index');


app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.set('json spaces', 1);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.get('/',index);
app.post('/userFileUpload', multipartMiddleware);
app.post('/userFileUpload', imageUpload);


http.createServer(app).listen(config.get('PORT'), function () {
    console.log("Server listening on port " + config.get('PORT'));
});
