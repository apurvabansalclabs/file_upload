/**
 * The node-module to hold the constants for the server
 */


function define(obj, name, value) {
    Object.defineProperty(obj, name, {
        value:        value,
        enumerable:   true,
        writable:     false,
        configurable: false
    });
}


exports.responseStatus = {};

define(exports.responseStatus, "PARAMETER_MISSING", 100);

define(exports.responseStatus, "ERROR_MESSAGE", 103);
define(exports.responseStatus, "SUCCESS_MESSAGE", 104);
define(exports.responseStatus, "FILE_NOT_SELECTED", 105);


exports.responseMessage = {}; define(exports.responseMessage, "PARAMETER_MISSING", "Some Parameters Missing");

define(exports.responseMessage, "ERROR_MESSAGE", "Something went wrong,Please try uploading again.");
define(exports.responseMessage, "SUCCESS_MESSAGE", "File Uploaded Successfully");
define(exports.responseMessage, "FILE_NOT_SELECTED", "File Not Selected");


