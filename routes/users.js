var express = require('express');
var router = express.Router();
var sendResponse = require('./sendResponse');
var commonFunction = require('./commonFunction');
var request = require("request");

router.post('/userFileUpload',function(req,res){
    var currentTime = new Date().getTime().toString();
    req.files.user_image.name=currentTime;
    
    commonFunction.imageUploadOnServer(req.files.user_image,'./Uploaded_files/',function (returnResult) {
        if(returnResult===0){
            sendResponse.somethingWentWrongError(res);
        }
        else if(returnResult===1){
            sendResponse.successStatusMsg(res);
        }
        else if(returnResult===2){
            sendResponse.fileNotSelected(res);
        }
    });

});
module.exports=router;