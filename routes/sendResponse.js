var constant = require('./constant');
exports.somethingWentWrongError = function (res) {
var errResponse = {
        status: constant.responseStatus.ERROR_MESSAGE,
        message: constant.responseMessage.ERROR_MESSAGE,
        data: {}
    };
    sendData(errResponse,res);

};

exports.successStatusMsg = function (res) {
var errResponse = {
        status: constant.responseStatus.SUCCESS_MESSAGE,
        message: constant.responseMessage.SUCCESS_MESSAGE,
        data: {}
    };
    sendData(errResponse,res);

};

exports.fileNotSelected = function (res) {
var errResponse = {
        status: constant.responseStatus.FILE_NOT_SELECTED,
        message: constant.responseMessage.FILE_NOT_SELECTED,
        data: {}
    };
    sendData(errResponse,res);

};
function sendData(data,res)
{
    res.type('json');
    res.jsonp(data);
}